//
//  ContentView.swift
//  TestGit
//
//  Created by Jean-Pierre on 30/03/2020.
//  Copyright © 2020 Jean-Pierre. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
            .font(.headline)
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
